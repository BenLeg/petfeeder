const express = require('express')
const app = express();
const mqtt = require('mqtt')
const schedule = require('node-schedule')
const fs = require('fs')

const db_file = "db_file.txt";

// SERVER CREATION ===================================

//Routing the resources available for the client on / path to the html folder
app.use('/',express.static(`${__dirname}/html`));

const server = app.listen(3000);
console.log("Web Server started on port 3000")
console.log("You can ctrl+left_click to open the client panel: http://localhost:3000");

// END SERVER CREATION ================================

var io = require('socket.io')(server); //Creating web-socket communication with clients

var mqtt_client  = mqtt.connect('mqtt://localhost:1883'); // connecting to the mqtt broker

var interface = { socket : {
	 	emit : () => {
	 		console.log("Socket not yet created");
	 	}
	}
};

//When the connection to the broker has succeeded
mqtt_client.on('connect', function () {
	//Subscription on the 'pet_feeder/pet_near' topic
	mqtt_client.subscribe('pet_feeder/pet_near', (err,data) => {
		if(err) throw err;
	});
})

//On the reception of new message from subscribed topics
mqtt_client.on('message', function (topic, message) {
	switch (topic) {
		//If the topic is pet_feeder/pet_near
		case "pet_feeder/pet_near":
			let distance = Number(message);
			if(distance < 30){
				fs.appendFile(db_file,new Date().getTime().toString()+"\n",function(err){
					if(err) {
						console.error(err);
						console.log("Data couldn't be saved");
					}
					// else console.log("Sauvegarde réussi");
				});
			}
			interface.socket.emit("pet_near",distance < 30);
			//console.log(`Pet near at ${new Date().toLocaleDateString()}: ${message}`);
			break;
		default:
			console.log(topic,message);
	}
})

//On connection by web sockets with client
io.on('connection', socket => {
	console.log("Client connected");

	interface.socket = socket;

	//On new event 'add_new_schedule'
	socket.on('add_new_schedule', (data) => {
		//Verification if all properties have good types and are present
		if(data.hasOwnProperty("date") && data.hasOwnProperty("amount_food")){
			//Adding a schedule job. Will be lost if the application crashes
			schedule.scheduleJob(new Date(data.date),function() {
				//The action will publish on the mqtt broker on the 'pet_feeder/feeding_activity' the number of dose of food to dispense
				mqtt_client.publish('pet_feeder/feeding_activity',`${data.amount_food}`);
			});
		}
	});

	//On new event 'start_feeding_activity'
	socket.on('start_feeding_activity',(amount_food) => {
		//publishes on the mqtt broker on the 'pet_feeder/feeding_activity' the number of dose of food to dispense
		mqtt_client.publish('pet_feeder/feeding_activity',`${amount_food}`);
	});

	//On new event 'get_pet_presence_history'
	socket.on('get_pet_presence_history',()=>{
		let presence_history = fs.readFileSync(db_file) //Read file
									.toString() //transforms buffer object returned to string
									.split("\n") //splits the string in an array of string every /n found
									.filter(elt => elt != ""); //Removes the last empty element

		//Sends back the history of presence of the pet
		socket.emit('res_pet_presence_history',presence_history);
	});

	//On new event 'disconnect'
	socket.on('disconnect', () => { console.log("Client disconnected"); });
});
