# Pet Feeder

LEGIGAN Benoît, LE GALL Corentin, HOULES Lucas

## Installation

Install node and npm in the first place in the server

Then, clone the project on this server :
```
git clone git@gitlab.com:BenLeg/petfeeder.git
```

Then, to install the module, just run those commands.
```
cd petfeeder
npm install
```

## Running

In a first console, run the command to start the broker
```
node broker.js
```

And in a second one, start the web server :
```
node app.js
```
In the console, a link to the web interface will be given.

__Important__: The python script will need to get the ip address of the broker (the server where you installed this application).
To get it, run this command :
```
ip addr
```

Then change it on the line number 134
