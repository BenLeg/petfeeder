$(document).ready(function() {
    var socket = io();

    $("#but_open_door").on('click',function() {
        amount_food_manual = $("#amount_food_manual").val();
        if (amount_food != ""){
            socket.emit('start_feeding_activity',amount_food_manual);
            $("#but_open_door").prop('disabled', true);
            setTimeout(function() {
                $("#but_open_door").prop('disabled', false);
            },amount_food_manual*2000)
        }
    });

    $("#but_schedule_register").on('click',function() {
        let date = $("#datepicker").val();
        let hours = $("#date_hours").val();
        let minutes = $("#date_minutes").val();
        let amount_food = $("#amount_food").val();

        date = new Date(date);
        date.setHours(hours);
        date.setMinutes(minutes);
        //console.log(date.toLocaleTimeString('FR'),date.toLocaleDateString('FR'),amount_food);
        date = date.getTime();

        socket.emit("add_new_schedule",{
            amount_food : amount_food,
            date : date
        });
    });

    //Creates the datepicker on the web interface (jquery-ui)
    $("#datepicker").datepicker();

    //Asks to the server the pet presence history
    socket.emit('get_pet_presence_history');

    //Updates the indicator if the pet is present.
    socket.on("pet_near",(pet_present) => {
        if(pet_present){
            $("#pet_presence").addClass("text-success").removeClass("text-danger");
            $("#pet_presence").children("span").text("Pet is near");
        }
        else {
            $("#pet_presence").addClass("text-danger").removeClass("text-success");
            $("#pet_presence").children("span").text("Pet is not here");
        }
    });

    //On receiving the pet presence history
    socket.on('res_pet_presence_history',(history) => {
        // DOM element where the Timeline will be attached
        var container = document.getElementById('visualization');

        // Transforms the history array in data compatible with the timeline
        // then creates a DataSet (allows two way data-binding)
        var items = new vis.DataSet(history.map( (elt, index) => {
            return {
                id : elt,
                start : new Date(Number(elt)),
                content: 'event ' + index
            };
        }));

        // console.log("histo length " + history.length)

        // Configuration for the Timeline
        var defaultOptions = {
            stack: true,
            height: '20em',
            type: 'point',
            min: new Date(Number(history[0] - 1000 * 60 * 60 * 24 * 7)), // first event minus one week
            max: new Date().setDate(new Date().getDate()+1), //tomorrow
            zoomMin: 1000 , // one second
            zoomMax: 1000 * 60 * 60 * 24 * 7, // one week in milliseconds
            template: (itemData, element, data) => {
                if (data.isCluster) {
                    return `<span class="cluster-header"</span><div>${data.items.length} events</div>`;
                } else {
                    return `<div>${data.content}</div>`;
                }
            }
        };

        // Create a Timeline
        var timeline = new vis.Timeline(container, items, defaultOptions);

        // cluster options
        var options = {}
        var clusterOpts = {
            cluster: {
                titleTemplate:
                    "Cluster containing {count} events. Zoom in to see the individual events.",
                showStipes: true
            }
        };
        Object.assign(options, defaultOptions, clusterOpts);

        timeline.setOptions(options);

    });
});
